﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace jlr_check_filenames
{
    class Program
    {
        private static string workDir;
        private static int warningCount;

        static void Main(string[] args)
        {
            var lastDate = new FileInfo(Assembly.GetExecutingAssembly().Location).LastWriteTimeUtc;

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine($"JLR Check Filenames | {lastDate.ToShortDateString()}");
            Console.ResetColor();

            if (args.Length > 0)
            {
                if (Directory.Exists(args[0])) workDir = args[0];
                else Terminate($"'{args[0]}' is not a valid directory.");
            }
            else
            {
                workDir = InputFolder();
            }

            workDir = workDir.Trim('"');

            if (!Directory.Exists(workDir))
                Terminate($"'{workDir}' is not a valid directory.");

            var files = Directory.GetFiles(workDir, "*.xml");

            Console.WriteLine($"\r\nChecking filenames in '{Path.GetFileName(workDir)}'.");

            if (files.Length == 0) Terminate("\r\nNo XML files in directory.");
            else Console.WriteLine($"\r\nCounting {files.Length} XML files in directory.\r\n");

            var variations = GetAllVariations(files);

            foreach (var file in files)
            {
                var fileName = Path.GetFileName(file);
                if (!CheckFileName(fileName))
                { 
                    Warning(fileName, "Not a valid file name.");
                }
                else
                {
                    var langCode = ParseLangCode(fileName);

                    if (langCode.ToLower() == Properties.Settings.Default.AuthoringLanguage.ToLower())
                        variations[ParseMarketAndType(file)] = true;

                    if (!CheckLangCode(langCode))
                        Warning(fileName, $"'{langCode}' is not a valid language code.");
                }
            }

            if (variations.ContainsValue(false))
            {
                foreach (var item in variations)
                {
                    if (!item.Value)
                        Warning(item.Key, $"Cannot detect '{Properties.Settings.Default.AuthoringLanguage.ToLower()}' file.");
                }
            }

            if (warningCount > 0)
            {
                string sAdd = warningCount > 1 ? "s" : "";
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"\r\nProgram finished with {warningCount} warning{sAdd}.");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"\r\nProgram finished without any warnings.");
            }
            Console.ResetColor();
            Console.WriteLine("\r\nPress any key to continue...");
            Console.ReadKey();
        }

        static void Terminate(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"\r\n{message}");
            Console.WriteLine($"\r\nTerminating program!");
            Console.ResetColor();
            Console.WriteLine("\r\nPress any key to continue...");
            Console.ReadKey();
            Environment.Exit(0);
        }
        static void Warning(string fileName, string message)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write($"{fileName}: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
            Console.ResetColor();
            warningCount++;
        }

        static string InputFolder()
        {
            string folder;

            while (true)
            {
                Console.WriteLine();
                Console.Write("Please input a folder: ");
                Console.ForegroundColor = ConsoleColor.White;
                folder = Console.ReadLine();
                Console.ResetColor();

                if (!string.IsNullOrWhiteSpace(folder)) break;
            }
            return folder;
        }

        static bool CheckFileName(string fileName)
        {
            var match = Regex.Match(fileName, Properties.Settings.Default.RegEx, RegexOptions.IgnoreCase);
            return match.Success;
        }
        static bool CheckLangCode(string langCode)
        {
            var allCultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
            return Array.Exists(allCultures, x => x.Name.ToLower() == langCode.ToLower());
        }
        static string ParseLangCode(string file)
        {
            var fileName = Path.GetFileNameWithoutExtension(file);
            return fileName.Substring(fileName.Length - 5);
        }

        static Dictionary<string, bool> GetAllVariations(string[] files)
        {
            var count = files.Length;
            var array = new string[count];
            var dict = new Dictionary<string, bool>();

            for (int i = 0; i < count; i++)
            {
                array[i] = ParseMarketAndType(files[i]);
            }

            foreach (var item in array.Distinct())
            {
                dict.Add(item, false);
            }
            return dict;
        }

        static string ParseMarketAndType(string file)
        {
            var fileName = Path.GetFileNameWithoutExtension(file);
            var market = fileName.Split(' ')[2].ToUpper();
            var type = fileName.Split(' ')[3].ToUpper();
            type = type.Substring(0, type.IndexOf('_'));
            return $"{market} {type}";
        }
    }
}